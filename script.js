class Employee {
  constructor(name, age, salary) {
    this._name = name;
    this._age = age;
    this._salary = salary;
  }
  get name() {
    return this._name;
  }
  set name(name) {
    this._name = name;
  }
  get age() {
    return this._age;
  }
  set age(age) {
    this._age = age;
  }
  get salary() {
    return this.salary;
  }
  set salary(salary) {
    this._salary = salary;
  }
}

class Programmer extends Employee {
  constructor(name, age, salary) {
    super(name, age, salary);
    this.lang;
  }
  get salary() {
    return this._salary * 3;
  }
}

let copies1 = new Programmer("Yaroslav", 20, 1000);
let copies2 = new Programmer("Volodymyr", 21, 2000);
let copies3 = new Programmer("Karina", 20, 3000);
console.log(copies1);
console.log(copies2);
console.log(copies3);

